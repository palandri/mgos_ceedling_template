#ifndef UNITY_TEMPLATE_h
#define UNITY_TEMPLATE_h

#ifdef TEST
#include "mgos.h"
#define MGOS_EVENT_BASE(a, b, c) ((a) << 24 | (b) << 16 | (c) << 8)
#include "mbuf.h"
#else
#include "mgos.h"
#include "mgos_event.h"
#endif

#include "stdint.h"
#include "stdbool.h"

#endif