#ifndef MGOS_UNITY_TEMPLATE_h
#define MGOS_UNITY_TEMPLATE_h

#ifdef TEST
#include "mgos.h"
#else
#include "mgos.h"
#endif

#include "stdbool.h"

bool mgos_unity_template_init(void);    // This is a wrapper for your init function, to be used during boot in MGOS, must be bool mgos_*_init(void)

#pragma once
#ifdef __cplusplus
extern "C"
{
#endif

bool unity_template_init(void);     //  Your init function, can be cpp

#ifdef __cplusplus
}

#endif
#endif