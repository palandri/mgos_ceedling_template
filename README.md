# Mongoose OS Unit Testing with Ceedling


## Overview

This repo is to be used as a template to integrate unit testing with Mongoose OS.

### How to use

Can be run over terminal with `ceedling clean && ceedling test:all`, or with VSCode ceedling plugin